import Vue from 'vue'
import App from './App.vue'

const bugsnag = require('bugsnag-js')
const bugsnagClient = bugsnag('878d1cafd94c14fc1e04e78b1f44da3e')
const bugsnagVue = require('bugsnag-vue')
bugsnagClient.use(bugsnagVue(Vue))

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
