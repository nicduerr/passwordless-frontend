# Passwordless Login POC Frontend

Vue.js frontend for [the passwordless backend POC](https://gitlab.com/nicduerr/passwordless-backend).
Live demo can be accessed at https://passwordless-poc.codewizard.io/

*Proof of concept only, not ready for production!*

## Local setup:
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
